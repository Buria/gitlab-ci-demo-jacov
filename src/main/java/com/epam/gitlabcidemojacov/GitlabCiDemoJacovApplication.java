package com.epam.gitlabcidemojacov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCiDemoJacovApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabCiDemoJacovApplication.class, args);
	}

}
